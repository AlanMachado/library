package br.com.library.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.library.model.Library;

@SuppressWarnings("unchecked")
@Transactional
@Repository
public class LibraryDaoImpl implements LibraryDao{
	
	@PersistenceContext
	private EntityManager em;
	private Session session;


	@Override
	public void insert(Library library) {
		em.persist(library);
	}

	@Override
	public Library find(Integer id) {
		return em.find(Library.class, id);
	}

	@Override
	public void remove(Library library) {
		em.remove(find(library.getId()));
	}

	@Override
	public void update(Library library) {
		em.merge(library);
	}

	@Override
	public List<Library> getAll() {
		getSession();
		Criteria criteria = session.createCriteria(Library.class);
		return criteria.list();
	}
	
	private void getSession(){
		session = em.unwrap(Session.class);
	}

}
