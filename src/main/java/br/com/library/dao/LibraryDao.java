package br.com.library.dao;

import br.com.library.model.Library;
import br.com.library.utils.Crud;

public interface LibraryDao extends Crud<Library>{

}
