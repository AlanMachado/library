package br.com.library.utils;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

public class FacesUtils {

	private FacesContext facesContext;

	public FacesUtils() {
		this.facesContext = FacesContext.getCurrentInstance();
	}
	
	public void addErrorMessage(String msg){
		FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, msg);
		facesContext.addMessage(null, facesMessage);
	}
	
	public void addSuccessMessage(String msg){
		FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, msg, msg);
		facesContext.addMessage(null, facesMessage);
	}
	
	
}
