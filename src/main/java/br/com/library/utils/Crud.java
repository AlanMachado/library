package br.com.library.utils;

import java.util.List;

public interface Crud<T> {

	void insert(T t);
	T find(Integer id);
	void remove(T t);
	void update(T t);
	List<T> getAll();
}
