package br.com.library.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.com.library.dao.LibraryDao;
import br.com.library.model.Library;
import br.com.library.utils.FacesUtils;

@Controller
@Scope("request")
public class LibraryBean {
	
	private Library library;
	private List<Library> libraries;
	
	@Autowired
	private LibraryDao libraryDao;
	
	private FacesUtils facesUtils;
	
	@PostConstruct
	public void init(){
		library = new Library();
		libraries = new ArrayList<Library>();
		facesUtils = new FacesUtils();
		listaAll();
	}
	
	public Library getLibrary() {
		return library;
	}

	public void setLibrary(Library library) {
		this.library = library;
	}
	
	public List<Library> getLibraries() {
		return libraries;
	}

	public void setLibraries(List<Library> libraries) {
		this.libraries = libraries;
	}

	public void create(){
		libraryDao.insert(library);
		facesUtils.addSuccessMessage("Library successfully created");
	}
	
	public void save(){
		libraryDao.update(library);
		facesUtils.addSuccessMessage("Library successfully updated");
	}
	
	public void remove(Library library){
		libraryDao.remove(library);
		listaAll();
		facesUtils.addSuccessMessage("Library successfully deleted");
	}
	
	public void listaAll(){
		libraries = libraryDao.getAll();
		
	}
	
}
