package br.com.library.controllers;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class LivroBean {
	
	private String livroNome;
	
	public void livroDigitado(){
		System.out.println("Nome do livro eh " + livroNome);
	}

	public String getLivroNome() {
		return livroNome;
	}

	public void setLivroNome(String livroNome) {
		this.livroNome = livroNome;
	}
	
	
}
